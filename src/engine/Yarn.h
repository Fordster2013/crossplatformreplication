#ifndef YARN_H_
#define YARN_H_

#include "GameObject.h"
#include "World.h"

class Yarn : public GameObject
{
public:
	CLASS_IDENTIFICATION('YARN', GameObject)

	enum EYarnReplicationState
	{
		ECRS_Pose = 1 << 0,
		ECRS_YarnId = 1 << 2,
		ECRS_PlayerID = 1 << 2,


		ECRS_AllState = ECRS_Pose | ECRS_YarnId | ECRS_PlayerID
	};

	static	GameObject* StaticCreate()
	{
		return new Yarn();
	}

	//Note - the code in the book doesn't provide this until the client.
	//This however limits testing.
	static	GameObjectPtr	StaticCreatePtr()
	{
		return GameObjectPtr(new Yarn());
	}

	void SetYarnId(uint32_t inYarnId)
	{
		mYarnId = inYarnId;
	}
	uint32_t GetYarnId() const
	{
		return mYarnId;
	}

	virtual uint32_t GetAllStateMask() const override
	{
		return ECRS_AllState;
	}

	void SetVelocity(const Vector3& inVelocity)
	{
		mVelocity = inVelocity;
	}
	
	const Vector3& GetVelocity()const
	{
		return mVelocity;
	}

	void SetPlayerId(int inPlayerId)
	{
		mPlayerId = inPlayerId;
	}

	int GetPlayerId()const
	{
		return mPlayerId;
	}

	void InitFromShooter(Player* inShooter);

	virtual bool HandleCollisionWithPlayer(Player* inPlayer)override;

	virtual void Update() override;

		//virtual void	Read( InputMemoryBitStream& inInputStream ) override;

	uint32_t Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const override;

	Yarn();
protected:
	Vector3 mVelocity;
	float mMuzzleSpeed;
	int mPlayerId;
private:
	uint32_t mYarnId;
};

typedef shared_ptr < Yarn >	YarnPtr;

#endif // YARN_H_
