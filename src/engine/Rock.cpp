#include "Rock.h"
#include "Maths.h"
#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"

Rock::Rock() :
	GameObject(), //Rock is a game object
	mRockId(0) //The rocks ID
{
	SetCollisionRadius(0.5f); //This is the collision radius for the rock, when it collides it can't run through it
}

void Rock::Update()
{
	
}

uint32_t Rock::Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const
{
	uint32_t writtenState = 0;

	if(inDirtyState & ECRS_RockId)
	{
		inOutputStream.Write((bool)true); //Setting the write to true
		inOutputStream.Write(GetRockId()); //When written get the rock id
		writtenState |= ECRS_RockId;
	}
	else
	{
		inOutputStream.Write((bool)false); //If rock cant write set it to false, as such it won't display
	}

	if(inDirtyState & ECRS_Pose)
	{
		inOutputStream.Write((bool)true);
		Vector3 location = GetLocation(); //Finds the rocks location
		inOutputStream.Write(location.mX); //Location along the X Axis
		inOutputStream.Write(location.mY); //Location along the Y Axis
		inOutputStream.Write(GetRotation()); //Gets the rocks rotation (Not sure we need this but didnt remove in case of errors)
		writtenState |= ECRS_Pose;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}

	return writtenState;
}