#include "Yarn.h"
#include "Maths.h"
#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"
#include "Player.h"
#include "Timing.h"

Yarn::Yarn() :
	GameObject(), //Yarn is a game object
	mYarnId(0), //The yarn ID
	mMuzzleSpeed(3.f),
	mVelocity(Vector3::Zero),
	mPlayerId(0)
{
	SetCollisionRadius(0.5f); //This is the collision radius for the yarn

}

void Yarn::Update()
{
	float deltaTime = Timing::sInstance.GetDeltaTime();

	SetLocation(GetLocation() + mVelocity * deltaTime);
}

uint32_t Yarn::Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const
{
	uint32_t writtenState = 0;

	if(inDirtyState & ECRS_YarnId)
	{
		inOutputStream.Write((bool)true); //Setting the write to true
		inOutputStream.Write(GetYarnId()); //When written get the yarn id
		writtenState |= ECRS_YarnId;
		
	}
	else
	{
		inOutputStream.Write((bool)false); //If yarn cant write set it to false, as such it won't display
	}

	if(inDirtyState & ECRS_Pose)
	{
		inOutputStream.Write((bool)true);
		Vector3 location = GetLocation(); //Finds the yarn location
		inOutputStream.Write(location.mX); //Location along the X Axis
		inOutputStream.Write(location.mY); //Location along the Y Axis

		Vector3 velocity = GetVelocity();
		inOutputStream.Write(velocity.mX);
		inOutputStream.Write(velocity.mY);

		inOutputStream.Write(GetRotation()); //Gets the yarn rotation (Not sure we need this but didnt remove in case of errors)
		writtenState |= ECRS_Pose;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}
	if (inDirtyState & ECRS_PlayerID)
	{
		inOutputStream.Write((bool)true);
		inOutputStream.Write(mPlayerId, 8);
		writtenState |= ECRS_PlayerID;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}

	return writtenState;


}

void Yarn::InitFromShooter(Player* inShooter) //Sets up the shooter
{
	SetPlayerId(inShooter->GetPlayerId()); //Sets the player ID
	Vector3 forward = inShooter->GetForwardVector(); //Shoots forward via a vector
	SetVelocity(inShooter->GetVelocity() + forward * mMuzzleSpeed); //Yarn Velocity 
	SetLocation(inShooter->GetLocation()); //Yarn Location
	SetRotation(inShooter->GetRotation()); //Yarn Rotation
}

bool Yarn::HandleCollisionWithPlayer(Player* inPlayer) //Handles the yarns collision with players
{
	(void)inPlayer;

	return false;
}