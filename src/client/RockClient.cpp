#include "RockClient.h"
#include "TextureManager.h"
#include "GameObjectRegistry.h"
#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"
#include "NetworkManagerClient.h"
#include "StringUtils.h"

RockClient::RockClient() :
	mTimeLocationBecameOutOfSync(0.f)
{
	mSpriteComponent.reset(new SpriteComponent(this));
	mSpriteComponent->SetTexture(TextureManager::sInstance->GetTexture("rock")); //Setting the Texture for the rock
}

void RockClient::Update()
{
	
}

void RockClient::Read(InputMemoryBitStream& inInputStream)
{
	bool stateBit;

	uint32_t readState = 0;

	inInputStream.Read(stateBit);
	if(stateBit)
	{
		uint32_t rockId;
		inInputStream.Read(rockId); //The RockID
		SetRockId(rockId); //Setting the Rock ID
		readState |= ECRS_RockId;
	}

	float oldRotation = GetRotation(); //Finding the old rotation and getting the new (Not sure this is needed as its a static obj but didnt wanna risk breaking)
	Vector3 oldLocation = GetLocation(); //Finding the old location and setting the new (Not sure this is needed as its a static obj but didnt wanna risk breaking)

	float replicatedRotation;
	Vector3 replicatedLocation;

	inInputStream.Read(stateBit);
	if(stateBit)
	{
		inInputStream.Read(replicatedLocation.mX);
		inInputStream.Read(replicatedLocation.mY);
		SetLocation(replicatedLocation);
		inInputStream.Read(replicatedRotation);
		SetRotation(replicatedRotation);
		readState |= ECRS_Pose;
	}
}
