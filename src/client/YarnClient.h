#ifndef YARN_CLIENT_H
#define YARN_CLIENT_H

#include "Yarn.h"
#include "SpriteComponent.h"
#include "Player.h"
#include "PlayerClient.h"

class YarnClient : public Yarn
{
public:
	static	GameObjectPtr	StaticCreate()		
	{ 
		return GameObjectPtr(new YarnClient()); 
	}

	virtual void Update();
	
	virtual void Read(InputMemoryBitStream& inInputStream) override;

	virtual bool HandleCollisionWithPlayer(Player* inPlayer)override;

protected:
	YarnClient();
private:
	float mTimeLocationBecameOutOfSync;

	SpriteComponentPtr	mSpriteComponent;
};
typedef shared_ptr<YarnClient> YarnClientPtr;
#endif //YARN_CLIENT_H
