#include "YarnClient.h"
#include "TextureManager.h"
#include "GameObjectRegistry.h"
#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"
#include "NetworkManagerClient.h"
#include "StringUtils.h"
#include "Player.h"
#include "PlayerClient.h"
#include "RenderManager.h"

YarnClient::YarnClient() :
	mTimeLocationBecameOutOfSync(0.f)
{
	mSpriteComponent.reset(new SpriteComponent(this));
	mSpriteComponent->SetTexture(TextureManager::sInstance->GetTexture("yarn")); //Setting the Texture for the yarn
}

void YarnClient::Update()
{
	
}

void YarnClient::Read(InputMemoryBitStream& inInputStream)
{
	bool stateBit;

	uint32_t readState = 0;

	inInputStream.Read(stateBit);
	if(stateBit)
	{
		Vector3 location;
		inInputStream.Read(location.mX);
		inInputStream.Read(location.mY);

		Vector3 velocity;
		inInputStream.Read(velocity.mX);
		inInputStream.Read(velocity.mY);
		SetVelocity(velocity);

		SetLocation(location + velocity * NetworkManagerClient::sInstance->GetRoundTripTime()); 

		float rotation;
		inInputStream.Read(rotation);
		SetRotation(rotation);

		uint32_t yarnId;
		inInputStream.Read(yarnId); //The YarnID
		SetYarnId(yarnId); //Setting the Yarn ID
		readState |= ECRS_YarnId;
	}

	float oldRotation = GetRotation(); //Finding the old rotation and getting the new (Not sure this is needed as its a static obj but didnt wanna risk breaking)
	Vector3 oldLocation = GetLocation(); //Finding the old location and setting the new (Not sure this is needed as its a static obj but didnt wanna risk breaking)

	float replicatedRotation;
	Vector3 replicatedLocation;

	inInputStream.Read(stateBit);
	if(stateBit)
	{
		inInputStream.Read(replicatedLocation.mX);
		inInputStream.Read(replicatedLocation.mY);
		SetLocation(replicatedLocation);
		inInputStream.Read(replicatedRotation);
		SetRotation(replicatedRotation);
		readState |= ECRS_Pose;
	}

	if (stateBit)
	{
		Vector3 color;
		inInputStream.Read(color);
		SetColor(color);
	}

	if (stateBit)
	{
		inInputStream.Read(mPlayerId, 8);
	}
}

bool YarnClient::HandleCollisionWithPlayer(Player* inPlayer) //Bool to handle collision with player
{
	if (GetPlayerId() != inPlayer->GetPlayerId())
	{
		RenderManager::sInstance->RemoveComponent(mSpriteComponent.get());
	}
	return false;
}

