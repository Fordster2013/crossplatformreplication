#include "YarnServer.h"
#include "ClientProxy.h"
#include "Timing.h"
#include "MoveList.h"
#include "Maths.h"
#include "PlayerServer.h"
#include "Player.h"
#include "Yarn.h"


YarnServer::YarnServer() 
{
	mTimeToDie = Timing::sInstance.GetFrameStartTime() + 1.f; //Time to die
}

void YarnServer::HandleDying()
{
	NetworkManagerServer::sInstance->UnregisterGameObject(this); //Handling the dying 
}

void YarnServer::Update()
{
	Yarn::Update(); //Updating Yarn In the server 

	if (Timing::sInstance.GetFrameStartTime() > mTimeToDie)
	{
		SetDoesWantToDie(true);
	}
}

bool YarnServer::HandleCollisionWithPlayer(Player* inPlayer) //Handles the yarns collision with the player
{
	if (inPlayer->GetPlayerId() != GetPlayerId())
	{
		SetDoesWantToDie(true);

		static_cast<PlayerServer*>(inPlayer)->TakeDamage(GetPlayerId()); //In the server setting player to take damage based om their playerID
	}
	return false;
}