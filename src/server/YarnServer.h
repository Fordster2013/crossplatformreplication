#ifndef YARN_SERVER_H
#define YARN_SERVER_H

#include "Yarn.h"
#include "NetworkManagerServer.h"


class YarnServer : public Yarn
{
public:
	static GameObjectPtr StaticCreate() 
	{ 
		return NetworkManagerServer::sInstance->RegisterAndReturn(new YarnServer()); //Setting up the register and return of the yarn
	}
	void HandleDying()override;

	virtual bool HandleCollisionWithPlayer(Player* inPlayer)override;

	virtual void Update() override;

protected:
	YarnServer();

private:
	float mTimeToDie;
};

#endif // YARN_SERVER_H
